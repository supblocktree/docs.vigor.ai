---
id: dev-tables
title: Tables
sidebar_label: Tables
---

## User table
*  **Loans Taken Out** Debt = Amount of Vigor loaned out to user
*  **Deposited Collateral** Collateral = Amount of collateral user has posted for loans (Amount of each token shown)
*  **Insurance Earnings (APR)** earnrate = 0.0; // annualized rate of return on user portfolio of insurance crypto assets, insuring for downside price jumps
*  Insurance = Amount of insurance user has posted (Amount of each token shown)
*  **Deposited Collateral Value (USD)** valueofcol = dollar value of user portfolio of collateral crypto assets
*  **Deposited Insurance Value (USD)** valueofins = dollar value of user portfolio of insurance crypto assets
*  **Loan Interest Rate (APR)** tesprice = annualized rate borrowers pay in periodic premiums to insure their collateral (Max 0.25, min 0.005 – This needs to be turned into a percentage x100)
*  pcts = percent contribution to solvency (weighted marginal contribution to risk (solvency) rescaled by sum of that
*  volcol = volatility of the user collateral portfolio
*  stresscol = model suggested percentage loss that the user collateral portfolio would experience in a stress event.
*  Istresscol = market determined implied percentage loss that the user collateral portfolio would experience in a stress event.
*  Svalueofcol = model suggested dollar value of the user collateral portfolio in a stress event.
*  Svalueofcole = model suggested dollar amount of insufficient collateral of a user loan in a stressed market.   Min((1 – svalueofcol ) * valueofcol – debt,0) 
*  Svalueofcoleavg = model suggested dollar amount of insufficient collateral of a user loan on average in down markets, expected loss
*  premiums = dollar amount of premiums borrowers would pay in one year to insure their collateral
*  feespaid = a payment of VIG that the borrower pays into the contract for taking out a loan
*  Borrower Credit Score creditscore = 500; //out of 800
*  Lastupdate = ? Last block updated??
*  Num of Late Insurance Payments Latepays = Amount of time loan premiums were paid late.

## Global table
*  solvency = 1.0; // solvency, represents capital adequacy to back the stablecoin
*  **Total Value of Collateral (USD)** valueofcol = 0.0; // dollar value of total portfolio of borrowers crypto collateral assets
*  **Total Value of Insurance (USD)** valueofins = 0.0; // dollar value of total portfolio of insurance crypto assets
*  scale = 1.0; // TES pricing model parameters are scaled to drive risk (solvency) to a target set by custodians.
*  svalueofcole = 0.0; // model suggested dollar value of the sum of all insufficient collateral in a stressed market SUM_i [ min((1 - svalueofcoli ) * valueofcoli - debti,0) ]
*  svalueofins = 0.0; // model suggested dollar value of the total insurance asset portfolio in a stress event. [ (1 - stressins ) * INS ]
*  stressins = 0.0; // model suggested percentage loss that the total insurance asset portfolio would experience in a stress event.
*  inreserve = whenever VIG is repaid into the system, a portion of this is sent into the reverse. The reserve is there in the event of any blck swans occurring
*  totaldebt = this is the amount of VIGOR that a user has borrowed. The user will make repayments for this debt using VIG
