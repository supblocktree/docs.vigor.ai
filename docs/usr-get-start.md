---
id: usr-get-start
title: Vigor Jungle Testnet Tutorial
sidebar_label: Getting Started
---

WHAT YOU NEED FOR THE TEST

## HOW TO CREATE AN ACCOUNT ON TESTNET

*  Go to [http://monitor.jungletestnet.io](http://monitor.jungletestnet.io/)
*  Click on **Create Key**
    * copy/save you pair 
*  Click on **Create Account**
    * Choose an **Account name** with 12 characters (a-z,1-5)
    *  Paste your keys
    *  Verify **reCaptcha**
    *  **Create**
*  Click on **Faucet**
    * Write your **Account name**
    * Verify **reCaptcha**
    * Click **Send Coins** (100 EOS will be credited to your account)
    * (don’t be scared by the red text. Everything is OK.)
    * Come back after 6 hours to claim more free Jungle EOS

## HOW TO SET UP SCATTER
* Download **Scatter** from [https://get-scatter.com/download](https://get-scatter.com/download)
* Open **Scatter**
* Go to **Administrative > Networks > Add custom network**

